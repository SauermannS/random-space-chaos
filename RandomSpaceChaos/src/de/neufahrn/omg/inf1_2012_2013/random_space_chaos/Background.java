package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

import java.text.DecimalFormat;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Klasse f�r den scrollenden Hintergrund. Verwaltet jeweils drei Hintergrundteile gleichzeitig und tauscht diese entsprechend des
 * sichtbaren Bereichs.
 * Hinweis: Hintergrundteile d�rfen nicht kleiner als Aufl�sung.X/2 sein!
 * 
 * @author Sascha
 */
public class Background {

	/**
	 * Pfad des Hintergrundbilds
	 */
	private final String backgroundImageName;

	/**
	 * Gibt die Scrollgeschwindigkeit des Hintergrunds an.
	 */
	private float scrollingSpeed = 1f;
	/**
	 * Sprite f�r den ersten Teil des Hintergrundbilds.
	 */
	private final Sprite sprite = new Sprite();

	/**
	 * Sprite f�r den zweiten Teil des Hintergrundbilds.
	 */
	private final Sprite sprite2 = new Sprite();

	/**
	 * Sprite f�r den dritten Teil des Hintergrundbilds.
	 */
	private final Sprite sprite3 = new Sprite();

	/**
	 * Welt des Hintergrunds
	 */
	private final World world;

	/**
	 * Letztes geladenes Hintergrundteil
	 */
	private int lastLoaded = 0;

	/**
	 * Anzahl der geladenen Hintergrundteile
	 */
	private final int partCount = 20;

	/**
	 * Gibt an wie viele Stellen in der Nummerierung der Hintergrundteile verwendet wurde. (vgl. Images/Resources)
	 * F�gt evtl. ben�tigte f�hrende Zimmern hinzu.
	 */
	private final DecimalFormat format = new DecimalFormat("00");

	/**
	 * Erstellt einen neuen scrollenden Hintergrund.
	 * 
	 * Benutze {@link #setScrollingSpeed(float)} um die Scrollgeschwindigkeit
	 * festzulegen.
	 * 
	 * @param world
	 *            Welt, die den Hintergrund geladen hat
	 * @param background
	 *            Pfad zum Hintergrundbild
	 */
	public Background(final World world, final String background)
	{
		this.world = world;
		backgroundImageName = background;

		loadNext(sprite);
		loadNext(sprite2);
		loadNext(sprite3);

		// Sprite 2 hinter (rechts von) 1 setzen und 3 hinter 2
		sprite2.setPosition(sprite.getX() + sprite.getWidth(), 0);
		sprite3.setPosition(sprite2.getX() + sprite2.getWidth(), 0);
	}

	/**
	 * Fragt die Scrollgeschwindigkeit ab.
	 * 
	 * @return Scrollgeschwindigkeit
	 */
	public float getScrollingSpeed()
	{
		return scrollingSpeed;
	}

	/*
	 * Zeichnet den Hintergrund auf den Screen
	 * 
	 * @param batch
	 * SpriteBatch des Spiels
	 */
	public void render(final SpriteBatch batch)
	{
		// Sprites zeichnen
		sprite.draw(batch);
		sprite2.draw(batch);
		sprite3.draw(batch);
	}

	/**
	 * Legt die Scrollgeschwindigkeit fest.
	 * 
	 * @param scrollingSpeed
	 *            Scrollgeschwindigkeit
	 */
	public void setScrollingSpeed(final float scrollingSpeed)
	{
		this.scrollingSpeed = scrollingSpeed;
	}

	/**
	 * Aktualisiert den Hintergrund, entfernt Hintergrundteile links vom Bildschirm und f�gt rechts neue Hintergrundteile hinzu.
	 * 
	 * @param input
	 *            Eingabe von externen Ger�ten
	 * @param delta
	 *            Zeit (in ms) seit der letzten Berechnung
	 */
	public void update(final Input input, final float delta)
	{
		// Hintergrundposition anpassen (scrollen)
		sprite.setX(sprite.getX() - (delta * scrollingSpeed));
		sprite2.setX(sprite2.getX() - (delta * scrollingSpeed));
		sprite3.setX(sprite3.getX() - (delta * scrollingSpeed));

		// Wenn Hintergrund 1 links aus Screen, hinter 3 setzen
		if ((sprite.getX() + sprite.getWidth()) < 0)
		{
			sprite.setX(sprite3.getX() + sprite3.getWidth());
			loadNext(sprite);
		}

		// Wenn Hintergrund 2 links aus Screen, hinter 1 setzen
		if ((sprite2.getX() + sprite2.getWidth()) < 0)
		{
			sprite2.setX(sprite.getX() + sprite.getWidth());
			loadNext(sprite2);
		}

		// Wenn Hintergrund 3 links aus Screen, hinter 2 setzen
		if ((sprite3.getX() + sprite3.getWidth()) < 0)
		{
			sprite3.setX(sprite2.getX() + sprite2.getWidth());
			loadNext(sprite3);
		}

	}

	/**
	 * L�d dem Sprite die n�chste Hintegrundtextur. Wenn der letzte Hintergrundteil (vgl. {@link #partCount}) �berschritten wird, wird von
	 * vorn begonnen.
	 * 
	 * @param s
	 *            Sprite
	 */
	private void loadNext(final Sprite s)
	{
		lastLoaded++;
		if (lastLoaded > partCount)
			lastLoaded = 1;
		final Texture texture = world.res.get(backgroundImageName + "Part" + format.format(lastLoaded), Texture.class);
		s.setRegion(texture);
		s.setBounds(s.getX(), s.getY(), texture.getWidth(), texture.getHeight());
	}

}
