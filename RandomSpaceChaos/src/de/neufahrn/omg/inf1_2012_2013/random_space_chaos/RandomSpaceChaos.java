package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Einstiegspunkt des Spiels. Erstellt die Welt und den SpriteBatch.
 * 
 * @author Sascha
 */
public class RandomSpaceChaos implements ApplicationListener {

	/**
	 * SpriteBatch, an den alle Renderaufgaben gegeben werden
	 */
	private SpriteBatch batch;

	/**
	 * Welt mit Objekten und Spieler
	 */
	private World world;

	/**
	 * Zeit seit dem letzten Tastendruck
	 */
	private long lastKeyPressTime;

	/**
	 * Erstellt einen neuen SpriteBatch und eine neue Welt.
	 */
	@Override
	public void create()
	{
		batch = new SpriteBatch();
		world = new World();
	}

	/**
	 * Gibt die Ressourcen des SpriteBatchs und der Welt wieder frei.
	 */
	@Override
	public void dispose()
	{
		batch.dispose();
		world.dispose();
	}

	@Override
	public void pause()
	{
	}

	/**
	 * Rendert die Welt und updated sie danach.
	 * Beim Drücken der Enter Taste wird das Spiel neugestartet.
	 */
	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 40, 40, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		world.render(batch);

		if (Gdx.input.isKeyPressed(Keys.ENTER) && ((lastKeyPressTime + 200000000) < TimeUtils.nanoTime()))
		{
			world.dispose();
			world = null;
			world = new World();

			lastKeyPressTime = TimeUtils.nanoTime();
		}
		world.update(Gdx.input, Gdx.graphics.getDeltaTime());
	}

	@Override
	public void resize(final int width, final int height)
	{
		// BUG: Eine veränderte Bildschirmauflösung sorgt bei anderem Seitenverhältnis dafür, dass AI bewegungen nicht mehr stimmen.
	}

	@Override
	public void resume()
	{
	}
}
