package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface IState {
	
	public void init();
	public void render(SpriteBatch batch);
	public void update(int delta);
	public int getID();
	
}
