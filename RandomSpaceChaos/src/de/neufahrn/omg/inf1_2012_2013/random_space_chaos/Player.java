package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.AI;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Collectable;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Collectable.Type;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectableDoubleLaser;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectableShield;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Moveable;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Moveable.MoveableType;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Moveable.Owner;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Projectile;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.ProjectileLaser;

/**
 * Spieler, welcher als Raumschiff auf dem Feld dargestellt wird, sowie manche Werte f�r die GUI.
 * 
 * @author Sascha
 * @author Michi
 * @author Niko
 * @author Jakob
 * @author Flo
 */
public class Player {

	/**
	 * Beschleunigung des Spielers (Vektor)
	 */
	private static Vector2 acceleration = new Vector2(5000, 7000);

	/**
	 * Reibungsverlust (Um zum Stillstand zu kommen)
	 */
	private static float dragForce = 0.09f;

	/**
	 * Maximale Geschwindigkeit (Maximale L�nge des Geschwindigkeitsvektors)
	 */
	private static float maxSpeed = 520f;

	/**
	 * Spieler gestorben?
	 */
	public boolean gameOver = false;

	/**
	 * Zeit (in ms) zwischen den einzelnen Sch�ssen
	 */
	private final float firerate = 0.15f;

	/**
	 * H�lle des Raumschiffs.
	 * Maximale H�lle wird in {@link #maxHull} festgelegt.
	 */
	private float hull = 100;

	/**
	 * Sprite des Raumschiffs.
	 */
	private Sprite img;

	/**
	 * Aufgesammeltes Item
	 */
	private Collectable Item;

	/**
	 * Verbleibende Leben
	 */
	private int lives = 3;
	/**
	 * Maximale H�lle (vgl. {@link #hull}) bei keiner Besch�digung.
	 */
	private final float maxHull = 100;
	/**
	 * Maximales Schild (vgl. {@link #shield}) bei vollst�ndiger Aufladung.
	 */
	private float maxShield = 100;

	/**
	 * Punkte
	 */
	private int points = 0;

	/**
	 * Position des Raumschiffs.
	 */
	private Vector2 pos = new Vector2();

	/**
	 * Schild des Raumschiffs.
	 * Maximales Schild wird in {@link #maxShield} festgelegt.
	 */
	private float shield = 100;
	/**
	 * Wert, um welchen das Schild bei jedem Tick regeneriert wird (in ms).
	 * Die Zeit zwischen der einzelnen Ticks wird in {@link #shieldRegenerationTickDelay} festgelegt.
	 */
	private final float shieldRegenerationPerTick = 1f;
	/**
	 * Zeit bis zum Start der Schildaufladung (in ms).
	 * Die verbleibende Zeit wird bei jedem Treffer des Spielers zur�ckgesetzt.
	 * Zeit in Sekunden.
	 */
	private final float shieldRegenerationStartDelay = 5.5f;
	/**
	 * Zeit zwischen den einzelnen Schild-Auflade-Ticks (in ms).
	 * Bei jedem Tick regeneriert das Schild um den in {@link #shieldRegenerationPerTick} festgelegten Wert.
	 * Zeit in Sekunden.
	 */
	private final float shieldRegenerationTickDelay = 0.09f;

	/**
	 * Zeit seit dem letzten Treffer des Spielers (in ms).
	 * Zeit in Sekunden.
	 */
	private float timeSinceLastHit = 0;
	/**
	 * Zeit seit dem letzten Schild-Regenerations-Ticks (in ms).
	 * Wenn Wert gleich oder gr��er als {@link #shieldRegenerationTickDelay} regeneriert das Schild.
	 * Zeit in Sekunden.
	 */
	private float timeSinceLastShieldRegenerationTick = 0;

	/**
	 * Zeit seit dem letzten Schuss (in ms).
	 * Wenn Wert gleich oder gr��er als {@link #firerate} kann wieder geschossen werden.
	 */
	private float timeSinceLastShot = 0;

	/**
	 * Geschwindigkeitsvektor des Raumschiffs.
	 */
	private final Vector2 velocity = new Vector2();

	// Funktioniert so nicht, braucht Weapon Klasse, da wir sonst immer nur den selben Schuss (Objekt) haben
	// List<Projectile> weapons=new ArrayList<Projectile>();
	// private final List<Weapon> weapons = new ArrayList<Weapon>();

	/**
	 * Gibt an, ob die Waffe gerade �berhitzt ist.
	 */
	private final boolean weaponOverheated = false;

	/**
	 * Temperatur der Waffe.
	 * �berhitzt beim �berschreiten von {@link #weaponTemperaturMax}.
	 * Wird jede ms um {@link #weaponTemperaturReduction} reduziert.
	 */
	private float weaponTemperatur = 0;

	/**
	 * Maximale Waffentemperatur vor �berhitzung.
	 */
	private final float weaponTemperaturMax = 5;

	/**
	 * Wert, um welchen {@link #weaponTemperatur} jede ms reduziert wird.
	 */
	private final float weaponTemperaturReduction = 0.09f;

	/**
	 * Welt, in der der Spieler existiert.
	 */
	private final World world;

	/**
	 * Restzeit des Shild Boosts
	 */
	private int boostTimeShield;

	/**
	 * Schild vor dem Boost
	 */
	private float boostBeforeShield;

	/**
	 * Ist DoubleLaser Boost aktiv?
	 */
	private boolean boosthasDoubleLaser;

	/**
	 * Ist Shield Boost aktiv?
	 */
	private boolean boosthasShieldBoost;

	/**
	 * Restzeit des DoubleLaser Boosts
	 */
	private int boostTimeDoubleLaser;

	/**
	 * Erstellt einen neuen Spieler an der angegebenen Position und l�d sein Bild.
	 * 
	 * @param pos
	 *            Startposition (Vektor)
	 * @param world
	 *            Welt
	 */
	public Player(final Vector2 pos, final World world)
	{
		this.world = world;
		final Texture texture = world.res.get("PlayerShip", Texture.class);

		setImg(new Sprite(texture));
		this.pos = pos;

		// final Weapon weaponLaser = new Weapon();
		// weaponLaser.projectiles.add(new ProjectileLaser(world));
		// weapons.add(weaponLaser);
		boostBeforeShield = maxShield;

	}

	/**
	 * F�gt dem Punktez�hler Punkte hinzu.
	 * 
	 * @param points
	 *            Punkte
	 */
	public void addPoints(final int points)
	{
		this.points += points;
	}

	/**
	 * Entscheidet, ob mit AI oder Collectable kollidiert wird und gibt den Methodenaufruf an die entsprechende Methode weiter.
	 * 
	 * @param target
	 *            Kollisionsziel
	 */
	public void collide(final Moveable target)
	{
		if (target.getIsClass() == MoveableType.AI)
			collideAI((AI) target);
		else if (target.getIsClass() == MoveableType.COLLECTABLE)
			collideCollectable((Collectable) target);
	}

	/**
	 * Gibt die Spielertextur wieder frei.
	 */
	public void dispose()
	{
		setImg(null);
	}

	/**
	 * Gibt den Sprite des Spieles zur�ck.
	 * 
	 * @return Sprite
	 */
	public Sprite getImg()
	{
		return img;
	}

	/**
	 * Zeichnet den Spieler auf dem Bildschirm
	 * 
	 * @param batch
	 */
	public void render(final SpriteBatch batch)
	{
		getImg().setPosition(pos.x, pos.y);
		getImg().draw(batch);
		world.font.draw(batch, "" + hull, 960, 113);
		world.font.draw(batch, "" + shield, 960, 40);
		world.font.draw(batch, "" + lives, 1140, 693);
		world.font.draw(batch, "" + points, 818, 693);
	}

	/**
	 * Legt den Sprite des Spielers fest.
	 * 
	 * @param img
	 *            Sprite
	 */
	public void setImg(final Sprite img)
	{
		this.img = img;
	}

	/**
	 * Aktualisiert die Position des Spielers.
	 * 
	 * @param input
	 *            Tastatur & Mauseingaben
	 * @param delta
	 *            Zeit seit der letzten Berechnung (in ms)
	 */
	public void update(final Input input, final float delta)
	{
		timeSinceLastHit += delta;
		lowerBoosts(delta);

		move(input, delta);
		shoot(input, delta);
		regenerateShield(delta);
	}

	/**
	 * Spieler nimmt Schaden und spielt den Hit Sound ab.
	 * 
	 * @param target
	 *            Kollisionsziel
	 */
	private void collideAI(final AI target)
	{
		takeDamage(target.colissionDamage);
		final Sound hit = world.res.get("SoundHit", Sound.class);
		hit.play();
	}

	/**
	 * Sammelt das Collectable ein, spielt den Pickup Sound ab und wendet den Effekt des Collectables an.
	 * 
	 * @param target
	 *            Kollisionsziel
	 */
	private void collideCollectable(final Collectable target)
	{
		if (target.type == Type.HULLREPAIR)
		{
			hull += target.strength;
			if (hull > maxHull)
				hull = maxHull;
		} else if (target.type == Type.POINTS)
			points += target.strength;
		else if (target.type == Type.LIFE)
			lives += target.strength;
		else if (target.type == Type.SHIELDBOOST)
		{
			final CollectableShield targetC = (CollectableShield) target;
			if (!boosthasShieldBoost)
			{
				boostBeforeShield = maxShield;
				maxShield = targetC.strength * maxShield;
			}
			shield = maxShield;
			boostTimeShield += targetC.getTime();
		} else if (target.type == Type.DOUBLELASER)
		{
			final CollectableDoubleLaser targetC = (CollectableDoubleLaser) target;
			boosthasDoubleLaser = true;
			boostTimeDoubleLaser += targetC.getTime();
		}

		final Sound pickup = world.res.get("SoundPickup", Sound.class);
		pickup.play();
	}

	/**
	 * Spieler verliert ein Leben und spielt passende Sounds ab.
	 * Wenn die Leben des Spiels auf 0 sinken, ist das Spiel vorbei und der Spieler wechselt seine Textur.
	 */
	private void looseLife()
	{
		final Sound random = world.res.get("SoundRandom", Sound.class);
		final Sound explosion = world.res.get("SoundExplosion", Sound.class);

		lives--;

		if (lives <= 0)
		{
			// Spiel vorbei, Spielergrafik auf "Explodiert" �nden.
			gameOver = true;
			final Texture texture = world.res.get("PlayerShipDestroyed", Texture.class);
			setImg(new Sprite(texture));
			explosion.play();
		} else
		{
			// Alle Gegner entfernen und Spieler Position+H�lle+Schild zur�cksetzen
			world.clearScreen();
			hull = maxHull;
			boostTimeShield = 0;
			maxShield = boostBeforeShield;
			shield = maxShield;
			pos = new Vector2(30, 720 / 2);
			random.play();
		}

	}

	private void lowerBoosts(final float delta)
	{
		boostTimeShield -= delta;
		boostTimeDoubleLaser -= delta;

		if (boostTimeShield <= 0)
		{
			boostTimeShield = 0;
			maxShield = boostBeforeShield;
			boosthasShieldBoost = false;
		}
		if (boostTimeDoubleLaser <= 0)
		{
			boostTimeDoubleLaser = 0;
			boosthasDoubleLaser = false;
		}

	}

	/**
	 * Bewegt den Spieler gem�� der Tastatureingaben, sorgt daf�r, dass der Spieler seine Maximalgeschwindigkeit nicht �berschreitet und
	 * nicht aus dem Bild fliegt.
	 * 
	 * @param input
	 *            Maus&Tastatureingaben
	 * @param delta
	 *            Zeit seit der letzten Berechung (in ms)
	 */
	private void move(final Input input, final float delta)
	{
		// TODO: Physikalisch korrekt berechnen
		if (input.isKeyPressed(Keys.RIGHT))
			// v1 = v0+a*t
			velocity.add(Player.acceleration.x * delta, 0);
		if (input.isKeyPressed(Keys.LEFT))
			velocity.sub(Player.acceleration.x * delta, 0);
		if (input.isKeyPressed(Keys.UP))
			velocity.add(0, Player.acceleration.y * delta);
		if (input.isKeyPressed(Keys.DOWN))
			velocity.sub(0, Player.acceleration.y * delta);
		velocity.sub(new Vector2(velocity.x * (Player.dragForce), velocity.y * (Player.dragForce)));
		velocity.limit(Player.maxSpeed);

		pos.add(velocity.x * delta, velocity.y * delta);

		// links
		if (pos.x < 0)
			pos.x = 0;

		// unten
		if (pos.y < 151)
			pos.y = 151;

		// rechts
		if (pos.x > (Gdx.graphics.getWidth() - getImg().getWidth()))
			pos.x = Gdx.graphics.getWidth() - getImg().getWidth();

		// oben
		if (pos.y > (642 - getImg().getHeight()))
			pos.y = 642 - getImg().getHeight();

	}

	/**
	 * Regeneriert das Schild um {@link #shieldRegenerationPerTick}, wenn seit dem letzten Treffer mindestens so viel Zeit vergangen ist,
	 * wie in {@link #shieldRegenerationStartDelay} festgelegt. Ebenfalls muss seit der letzten Regeneration mindestens so viel Zeit, wie in
	 * {@link #shieldRegenerationTickDelay} festgelegt, vergangen sein.
	 * 
	 * @param delta
	 *            Zeit seit der letzten Berechnung (in ms)
	 */
	private void regenerateShield(final float delta)
	{
		if (timeSinceLastHit > shieldRegenerationStartDelay)
		{
			timeSinceLastShieldRegenerationTick += delta;
			if (timeSinceLastShieldRegenerationTick > shieldRegenerationTickDelay)
			{
				shield += shieldRegenerationPerTick;

				timeSinceLastShieldRegenerationTick -= shieldRegenerationTickDelay;
			}
		}
		if (shield > maxShield)
			shield = maxShield;
	}

	/**
	 * Schie�t ein Projektil, wenn SPACE gedr�ckt ist, seit dem letzten Schuss ({@link #timeSinceLastShot} mehr Zeit vergangen ist, als in
	 * {@link #firerate} festgelegt wurde, und die Waffe nicht �berhitzt ist. Die Waffe �berhitzt, wenn {@link #weaponTemperatur} gr��er als
	 * {@link #weaponTemperaturMax} ist. Bei jedem durchgef�hrten Schuss wird der Schuss Sound abgespielt und ein Projektil mittig vor dem
	 * Spieler gespawnt.
	 * 
	 * @param input
	 *            Maus&Tastatureingaben
	 * @param delta
	 *            Zeit seit der letzten Berechnung (in ms)
	 */
	private void shoot(final Input input, final float delta)
	{
		timeSinceLastShot += delta;
		if (input.isKeyPressed(Keys.SPACE) && (timeSinceLastShot > firerate) && (weaponTemperatur <= weaponTemperaturMax))
		{
			// TODO: Liest nur das erste Projektil aus + Positionsanpassung je nach Weapon
			// Projectile laser = weapons.get(selectedWeapon).projectiles.get(0);
			final Projectile laser = new ProjectileLaser(world);
			laser.texture = world.res.get("ProjectileLaserBlue", Texture.class);
			laser.sprite = new Sprite(laser.texture);
			laser.owner = Owner.PLAYER;
			laser.colissionDamage = 20;
			laser.pos.x = (pos.x + getImg().getWidth()) - (laser.sprite.getWidth() / 4);

			if (!boosthasDoubleLaser)
				laser.pos.y = (pos.y + (getImg().getHeight() / 2)) - (laser.sprite.getHeight() / 2);
			else
			{
				final Projectile laser2 = new ProjectileLaser(world);
				laser2.texture = world.res.get("ProjectileLaserBlue", Texture.class);
				laser2.sprite = new Sprite(laser.texture);
				laser2.owner = Owner.PLAYER;
				laser2.colissionDamage = 20;
				laser2.pos.x = (pos.x + getImg().getWidth()) - (laser.sprite.getWidth() / 4);

				laser.pos.y = (pos.y + (getImg().getHeight() / 2)) - ((laser.sprite.getHeight() / 2) + (getImg().getHeight() / 4));
				laser2.pos.y = (pos.y + (getImg().getHeight() / 2)) - ((laser2.sprite.getHeight() / 2) - (getImg().getHeight() / 4));
				world.spawnMoveable(laser2);
			}
			world.spawnMoveable(laser);
			timeSinceLastShot = 0;
			weaponTemperatur += delta;

			final Sound laserS = world.res.get("SoundLaser", Sound.class);
			laserS.play();
		}
		// BUG: WeaponCooldown funktioniert nicht/merkt man nicht?
		weaponTemperatur -= delta * weaponTemperaturReduction;
	}

	/**
	 * Spieler nimmt um den angegebenen Wert Schaden. Der Spieler verliert zuerst sein Schild und dann die H�lle.
	 * Wenn die H�lle unter/auf 0 geht, verliert der Spieler ein Leben.
	 * 
	 * @param damage
	 *            Schaden
	 */
	private void takeDamage(final float damage)
	{
		final float damageForHull = shield - damage;
		if (damageForHull < 0)
		{
			hull += damageForHull;
			shield = 0;
		} else
			shield -= damage;
		timeSinceLastHit = 0;

		if (hull <= 0)
			looseLife();

	}

}
