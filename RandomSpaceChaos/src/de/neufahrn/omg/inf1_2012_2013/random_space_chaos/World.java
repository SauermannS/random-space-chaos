package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Collectable;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectableDoubleLaser;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectableLife;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectablePoints;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectableRepair;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.CollectableShield;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.EnemyBird;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.GUI;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Moveable;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Moveable.Owner;

/**
 * Die Welt verwaltet alle Objekte auf dem Bildschirm (z.B Spieler, Gegner, GUI), aktualisiert diese (z.B. Position) und zeichnet sie auf
 * dem Bildschirm.
 * Sie l�d ebenfalls alle ben�tigten Resourcen (Sounds, Bilder) vor und k�mmert sich um die Musik.
 * 
 * @author Sascha
 * @author Flo
 */
public class World {

	/**
	 * Fabrik f�rs spawnen von Collectables
	 */
	public Factory<Collectable> factoryCol;

	/**
	 * Schriftart f�r Textdarstellung
	 */
	public BitmapFont font;

	/**
	 * ResourceLoader zum Laden der ben�tigten Bilder/Sounds/Musik
	 */
	public ResourceLoader res;

	/**
	 * Spieler
	 */
	public Player Spieler;

	/**
	 * Hintergrundmusik
	 */
	private final Music backgroundMusic;

	/**
	 * Scrollender Hintergrund
	 */
	private final Background bg;

	/**
	 * Kamera
	 */
	private final OrthographicCamera cam;

	/**
	 * Sichtbarkeit der Debuginformationen
	 */
	private boolean debugmode = false;

	/**
	 * Liste der GUI-Objekte auf dem Bildschirm.
	 */
	private final List<GUI> GUIList;

	/**
	 * Zeit seit dem letzten Tastendruck (in ms)
	 */
	private long lastKeyPressTime;

	/**
	 * Liste der Moveable-Objekte auf dem Bildschirm.
	 */
	private final List<Moveable> moveableList;

	// ConcurrentModificationException ohne 2. Liste, da Erweiterung der Liste,
	// w�hrend Abarbeitung
	/**
	 * Liste der beim n�chsten Update hinzuzuf�genden Moveables
	 */
	private final List<Moveable> moveablesToAdd;

	/**
	 * Liste der beim n�chsten Update zu entfernenden Moveables
	 */
	private final List<Moveable> moveablesToRemove;

	/**
	 * Spiel pausiert?
	 */
	private boolean pause = false;

	/**
	 * Scrollgeschwindigkeit des Hintergrunds
	 */
	private final float ScrollingSpeed = 120f;

	/**
	 * Zeit seit Start des Spiels
	 */
	private float time = 0;

	/**
	 * Zeit seit dem letzten Gegnerspawn (in ms)
	 */
	private long timeLastSpawn;

	/**
	 * Schwierigkeitsgrad des Gegnerspawns
	 */
	private int difficulty = 1;

	/**
	 * Erstellt eine neue Welt, l�d Ressourcen, und startet die Hintergrundmusik.
	 */
	public World()
	{
		cam = new OrthographicCamera();
		cam.setToOrtho(false, 1280, 720);

		res = new ResourceLoader("resources.xml");
		res.load();
		while (res.update())
		{
			// TODO: Sch�ner l�sen mit Ladescreen
		}

		bg = new Background(this, "Background");
		bg.setScrollingSpeed(ScrollingSpeed);
		Spieler = new Player(new Vector2(30, 720 / 2), this);
		moveableList = new ArrayList<Moveable>();
		moveablesToAdd = new ArrayList<Moveable>();
		moveablesToRemove = new ArrayList<Moveable>();
		GUIList = new ArrayList<GUI>();
		font = new BitmapFont();

		factoryCol = new Factory<Collectable>(this);

		final SpawnChance<CollectablePoints> scCP = new SpawnChance<CollectablePoints>(CollectablePoints.class, 20);
		factoryCol.addSpawnChance(scCP);
		final SpawnChance<CollectableRepair> scCR = new SpawnChance<CollectableRepair>(CollectableRepair.class, 10);
		factoryCol.addSpawnChance(scCR);
		final SpawnChance<CollectableShield> scCS = new SpawnChance<CollectableShield>(CollectableShield.class, 15);
		factoryCol.addSpawnChance(scCS);
		final SpawnChance<CollectableLife> scCL = new SpawnChance<CollectableLife>(CollectableLife.class, 3);
		factoryCol.addSpawnChance(scCL);
		final SpawnChance<CollectableDoubleLaser> scCDL = new SpawnChance<CollectableDoubleLaser>(CollectableDoubleLaser.class, 15);
		factoryCol.addSpawnChance(scCDL);

		backgroundMusic = res.get("MusicSpaceBackground", Music.class);
		backgroundMusic.setLooping(true);
		backgroundMusic.play();
	}

	/**
	 * L�scht alle Moveables vom Screen.
	 */
	public void clearScreen()
	{
		for (final Moveable m : moveableList)
			moveablesToRemove.add(m);
	}

	/**
	 * Gibt alle Ressourcen wieder frei.
	 */
	public void dispose()
	{
		backgroundMusic.stop();
		res.dispose();
		// Moveables rendern
		for (final Moveable m : moveableList)
			m.dispose();

		// Spieler rendern
		Spieler.dispose();

		// GUI rendern
		for (final GUI gui : GUIList)
			gui.dispose();
	}

	/**
	 * Zeichnet den Hintergrund, die Moveables, die GUI, den Spieler und ggf. Texteinblendungen.
	 * 
	 * @param batch
	 *            SpriteBatch
	 */
	public void render(final SpriteBatch batch)
	{
		batch.begin();

		// Kamera aktualisieren
		cam.update();
		batch.setProjectionMatrix(cam.combined);

		// Hintergrundbild rendern
		bg.render(batch);

		// GUI-Bild rendern
		final Texture guiImg = res.get("Interface", Texture.class);
		batch.draw(guiImg, 0, 0);

		// Moveables rendern
		for (final Moveable m : moveableList)
			m.render(batch);

		// Spieler rendern
		Spieler.render(batch);

		// GUI-Elemente rendern
		for (final GUI gui : GUIList)
			gui.render(batch);

		if (debugmode)
			showDebugInfos(batch);

		if (pause)
			font.draw(batch, "GAME PAUSED - Press ESC to continue", (Gdx.graphics.getWidth() / 3) + 80, Gdx.graphics.getHeight() - 100);

		if (Spieler.gameOver)
			font.draw(batch, "GAME OVER - Press ENTER to restart", (Gdx.graphics.getWidth() / 3) + 130, Gdx.graphics.getHeight() / 2);

		font.draw(batch, "Time: " + ((float) Math.round((time) * 100) / 100) + "s", 450, 693);
		font.draw(batch, "MADE BY:", 20, 120);
		font.draw(batch, "Marco Thalmeier, Sascha Sauermann,", 20, 90);
		font.draw(batch, "Florian Eckart-W., Stefan Gerhardinger,", 20, 70);
		font.draw(batch, "Jakob Brandmaier, Nico Tomiczek,", 20, 50);
		font.draw(batch, "Michael Junker", 20, 30);

		font.draw(batch, "Random Space Chaos - Q11 Inf1 2012-2013", 15, 693);

		font.draw(batch, "Version: Alpha 0.3", 200, 120);

		font.draw(batch, "Difficulty: " + difficulty, 535, 40);

		batch.end();
	}

	/**
	 * Markiert ein Objekt zum Aktualisieren und Rendern.
	 * Zum L�schen benutze: {@link #despawnMoveable(Moveable)}
	 * 
	 * @param e
	 *            Objekt (Moveable)
	 */
	public void spawnMoveable(final Moveable e)
	{
		moveablesToAdd.add(e);
	}

	/**
	 * Aktualisiert alle Objekte auf dem Bildschirm wie den Spieler oder Gegner, solange das Spiel nicht pausiert oder verloren ist.
	 * Zeigt Debuginformationen beim Dr�cken auf die TAB Taste.
	 * Pausiert das Spiel beim Dr�cken der ESC Taste.
	 * 
	 * @param input
	 *            Eingaben von Steuerger�ten (z.B. Tastatur)
	 * @param delta
	 *            Zeit (in ms) seit Start der letzten Ausf�hrung
	 */
	public void update(final Input input, final float delta)
	{

		// Spiel pausieren
		if (input.isKeyPressed(Keys.ESCAPE) && ((lastKeyPressTime + 200000000) < TimeUtils.nanoTime()))
		{
			if (pause)
				pause = false;
			else
				pause = true;

			lastKeyPressTime = TimeUtils.nanoTime();
		}

		// Wenn Spiel nicht pausiert oder Spieler GameOver ist
		if (!pause && !Spieler.gameOver)
		{

			time += delta;
			difficulty = (int) (time / 20) + 1;

			// Music starten, wenn sie noch nicht l�uft (Wird durch Pause gestoppt)
			if (!backgroundMusic.isPlaying())
				backgroundMusic.play();

			// Hintergrund aktualisieren
			bg.update(input, delta);

			// Hier Entities hinzuf�gen
			spawn();

			// Zu entfernende Moveables aus Liste l�schen
			for (final Moveable m : moveablesToRemove)
				moveableList.remove(m);
			moveablesToRemove.clear();
			for (final Moveable m : moveablesToAdd)
				moveableList.add(m);
			moveablesToAdd.clear();

			// Matrix, um zu speichern, welche Kombinationen bei der Kollisions�berpr�fung schon beachtet wurden, um mehrfache �berpr�fungen
			// auszuschlie�en.
			final Boolean[][] collisionChecked = new Boolean[moveableList.size()][moveableList.size()];
			// Moveables aktualisieren, wenn sie nicht gel�scht werden sollen.
			for (final Moveable m : moveableList)
				if (m.isDeleted())
					despawnMoveable(m);
				else
				{
					m.update(input, delta);
					collisionTest(m, collisionChecked);
				}

			// Spieler aktualisieren
			Spieler.update(input, delta);
			collisionTestPlayer(Spieler);

			// GUI aktualisieren
			for (final GUI gui : GUIList)
				gui.update(input, delta);

		} else
			backgroundMusic.pause();

		if (input.isKeyPressed(Keys.TAB))
			toggleDebugmode();

	}

	/**
	 * Testet, ob ein Objekt aus {@link #moveableList} mit irgendeinem anderen kollidiert.
	 * 
	 * @param m
	 *            Zu testendes Objekt (Moveable)
	 * @param collisionChecked
	 *            Matrix mit bereits gepr�ften Kombinationen
	 */
	private void collisionTest(final Moveable m, final Boolean[][] collisionChecked)
	{
		collisionChecked[moveableList.indexOf(m)][moveableList.indexOf(m)] = true;
		for (final Moveable testM : moveableList)
		{
			if ((m.collisionType != testM.collisionType) && (m.owner != testM.owner) && (testM.isDeleted() == false)
					&& (m.isDeleted() == false))
				if (m.sprite.getBoundingRectangle().overlaps(testM.sprite.getBoundingRectangle()))
				{
					m.collide(testM);
					testM.collide(m);
				}

			collisionChecked[moveableList.indexOf(m)][moveableList.indexOf(testM)] = true;
			collisionChecked[moveableList.indexOf(testM)][moveableList.indexOf(m)] = true;
		}
	}

	/**
	 * Testet, ob ein Objekt aus {@link #moveableList} mit einem Spieler kollidiert.
	 * 
	 * @param p
	 *            Spieler
	 */
	private void collisionTestPlayer(final Player p)
	{
		for (final Moveable testM : moveableList)
			if ((testM.owner != Owner.PLAYER) && (testM.isDeleted() == false))
				if (p.getImg().getBoundingRectangle().overlaps(testM.sprite.getBoundingRectangle()))
				{
					p.collide(testM);
					testM.collide(p);
				}
	}

	/**
	 * Markiert das Objekt als "Zu l�schen"
	 * Zum Hinzuf�gen benutze: {@link #spawnMoveable(Moveable)}
	 * 
	 * @param m
	 *            Das zu markierende Objekt
	 */
	private void despawnMoveable(final Moveable m)
	{
		moveablesToRemove.add(m);
	}

	/**
	 * Rendert die Debuginformationen, wenn die angezeigt werden sollen.
	 * 
	 * @param batch
	 *            SpriteBatch
	 */
	private void showDebugInfos(final SpriteBatch batch)
	{
		font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 5, Gdx.graphics.getHeight() - 70);
		font.draw(batch, "Entities: " + (1 + moveableList.size() + GUIList.size()), 5, Gdx.graphics.getHeight() - 85);
		// font.draw(batch, "Position: "+
		// (float)Math.round(Spieler.pos.x*100)/100 +", " +
		// (float)Math.round(Spieler.pos.y*100)/100, 5,
		// Gdx.graphics.getHeight()-50);
		// font.draw(batch, "Speed: "+
		// (float)Math.round(Spieler.velocity.len()*100)/100 + " (" +
		// (float)Math.round(Spieler.velocity.x*100)/100 +", " +
		// Math.round(Spieler.velocity.y*100) + ")", 5,
		// Gdx.graphics.getHeight()-70);
		// font.draw(batch, "Weapon Reload Time: "+
		// (float)Math.round((Spieler.firerate-Spieler.timeSinceLastShot)*100)/100,
		// 5, Gdx.graphics.getHeight()-90);
		// font.draw(batch, "No Hit Time: "+
		// (float)Math.round(Spieler.timeSinceLastHit*100)/100, 5,
		// Gdx.graphics.getHeight()-110);
		// font.draw(batch, "Time Until Shield Regeneration: "+
		// (float)Math.round((Spieler.shieldRegenerationStartDelay-Spieler.timeSinceLastHit)*100)/100,
		// 5, Gdx.graphics.getHeight()-130);
	}

	/**
	 * Spawnt einen Gegner
	 */
	private void spawn()
	{
		final float height = Gdx.graphics.getHeight();
		if (((TimeUtils.millis() - timeLastSpawn) > (8000 - (difficulty * 60) - (Math.random() * 5000) - 1)))
		{
			final EnemyBird e = new EnemyBird(this);
			e.pos.y = (float) ((Math.random() * (((7 * height) / 8) - ((2 * height) / 8))) + ((2 * height) / 8));
			e.pos.x = Gdx.graphics.getWidth() + 30;
			e.owner = Owner.ENEMY;
			e.health = e.health * 0.3f * difficulty;
			e.colissionDamage = e.colissionDamage * 0.5f * difficulty;
			e.attackDamage = e.attackDamage * 0.5f * difficulty;
			e.timeLastShot = e.timeLastShot - (100 * difficulty);
			e.maxSpeed = e.maxSpeed + (30 * difficulty);
			spawnMoveable(e);

			timeLastSpawn = TimeUtils.millis();
		}
	}

	/**
	 * Wechselt die Sichtbarkeit der Debuginformationen
	 */
	private void toggleDebugmode()
	{
		if (TimeUtils.nanoTime() > (lastKeyPressTime + 200000000))
		{
			if (debugmode)
				debugmode = false;
			else
				debugmode = true;

			lastKeyPressTime = TimeUtils.nanoTime();
		}
	}

}
