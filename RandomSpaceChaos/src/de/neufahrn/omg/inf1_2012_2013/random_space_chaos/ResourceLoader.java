package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader;

/**
 * L�d Ressourcen/Dateien von der Festplatte und h�lt sie zur sp�teren Verwendung im RAM vor.
 * 
 * @author Sascha
 * 
 */
public class ResourceLoader {
	/**
	 * AssetManager zum Laden der Ressourcen
	 */
	private final AssetManager manager = new AssetManager();
	/**
	 * XmlReader zum Laden der XML Datei
	 */
	private final XmlReader xml = new XmlReader();
	/**
	 * Oberster Knoten der XML Datei
	 */
	private XmlReader.Element rootNode;
	/**
	 * Map zum Speichern von Ressourcen-ID und -Pfad
	 */
	private final Map<String, String> resources = new HashMap<String, String>();

	/**
	 * Erstellt einen neuen ResourceLoader und parst die XML-Datei.
	 * 
	 * @param ResourceXML
	 *            Pfad zur XML-Datei
	 * @throws IOException
	 */
	public ResourceLoader(final String ResourceXML)
	{
		try
		{
			rootNode = xml.parse(Gdx.files.internal(ResourceXML));
		} catch (final IOException e)
		{
			rootNode = null;
			e.printStackTrace();
		}
	}

	/**
	 * Gibt die verwendeten Ressourcen frei und stoppt weiteres Laden.
	 */
	public void dispose()
	{
		manager.dispose();
	}

	/**
	 * Gibt die Ressource mit der angegebenen ID zur�ck.
	 * 
	 * @param id
	 *            ID/Name der Ressource (festgelegt in der geladenen XML-Datei - vgl. {@link #ResourceLoader(String)})
	 * @param type
	 *            Typ/Klasse der Ressource (z.B. {@code Class.class})
	 * @return Ressource (Datentyp wie bei {@code type} angegeben)
	 */
	public <T> T get(final String id, final java.lang.Class<T> type)
	{
		return manager.get(resources.get(id), type);
	}

	/**
	 * L�d alle Ressourcen aus der geladenen XML-Datei (vgl. {@link #ResourceLoader(String)}).
	 * Um eine Ressource zu bekommen siehe {@link #get(String, Class)}.
	 * 
	 * @throws ClassNotFoundException
	 */
	public void load()
	{
		// Texturfilter setzen
		final TextureParameter params = new TextureParameter();
		params.genMipMaps = true;
		params.minFilter = TextureFilter.MipMapLinearNearest;
		params.magFilter = TextureFilter.Nearest;
		// F�r jeden Bereich (Texturen, Sound, Music) Dateien laden
		for (int i = 0; i < rootNode.getChildCount(); i++)
		{
			final XmlReader.Element node = rootNode.getChild(i);
			for (final XmlReader.Element file : node.getChildrenByName("file"))
			{
				// FEATURE: Klassenunabhangiges Laden (--> Klasse direkt aus Textname)
				final String type = node.getName();
				if (type.equalsIgnoreCase("Texture"))
					manager.load(file.get("path"), Texture.class, params);
				else if (type.equalsIgnoreCase("Sound"))
					manager.load(file.get("path"), Sound.class);
				else if (type.equalsIgnoreCase("Music"))
					manager.load(file.get("path"), Music.class);
				else
					throw new GdxRuntimeException("Resource Type not found: '" + type + "'");
				// Datei in Map aufnehmen
				resources.put(file.get("id"), file.get("path"));
			}
		}
	}

	/**
	 * Gibt an, ob noch Ressourcen geladen werden.
	 * 
	 * @return Falsch, wenn fertig geladen
	 */
	public boolean update()
	{
		return !manager.update();
	}

}
