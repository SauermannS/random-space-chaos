package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

/**
 * Klasse beschreibt die Wahrscheinlich einzelner Objekte zu spawnen und l�sst sie ggf. spawnen.
 * 
 * @author Sascha
 * @param <T>
 *            Klasse
 */
public class SpawnChance<T> {
	/**
	 * Spawnwahrscheinlichkeit in Prozent (%)
	 */
	private final int chance;
	/**
	 * Zu spawnendes Objekt
	 */
	private final Class<T> className;

	/**
	 * Stellt die Spawnwahrscheinlichkeit eines Objekts dar.
	 * 
	 * 
	 * @param clazz
	 *            Klasse/Moveable
	 * @param chance
	 *            Wahrscheinlichkeit zu spawnen in Prozent (%)
	 */
	public SpawnChance(final Class<T> clazz, final int chance)
	{
		className = clazz;
		this.chance = chance;
	}

	/**
	 * Fragt die Klasse des Moveables ab
	 * 
	 * @return Klasse/Moveable
	 */
	public Class<T> getClassName()
	{
		return className;
	}

	/**
	 * Soll das Objekt gespawnt werden?
	 * 
	 * @return Wahr/Falsch
	 */
	public boolean shouldSpawn()
	{
		if ((Math.random() * 100) < chance)
			return true;
		return false;
	}
}
