package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import java.util.List;

import com.badlogic.gdx.Input;

/**
 * Von dieser Klasse erben alle Gegner-Objekte
 * 
 * @author Nico
 * @author Michael
 * @author Sascha
 */
public abstract class Enemy extends AI {

	// TODO dont work vgl. Spieler

	/**
	 * In dieser Liste stehen die Waffentypen der einzelnen Gegner-Objekte
	 */
	public List<Projectile> weapon;

	public float attackDamage;

	/**
	 * Leerer Konstruktor
	 */
	public Enemy()
	{
		super();
	}

	/**
	 * Aktualisiert die Position der Objekte, bewegt sie und l�sst sie schie�en.
	 */
	@Override
	public void update(final Input input, final float delta)
	{
		move(delta);
		shoot(delta);
		super.update(input, delta);
	}

	/**
	 * Bewegt ein Objekt.
	 * 
	 * @param delta
	 *            Zeit seit der letzten Berechnung (in ms)
	 */
	protected abstract void move(float delta);

	/**
	 * L�asst ein Objekt schie�en.
	 * 
	 * @param delta
	 *            Zeit seit der letzten Berechnung
	 */
	protected abstract void shoot(float delta);

}
