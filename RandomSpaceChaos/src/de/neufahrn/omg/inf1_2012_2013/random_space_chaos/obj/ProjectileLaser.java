package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * Klasse f�r ein Laser-Projektil.
 * 
 * @author Sascha
 */
public class ProjectileLaser extends Projectile {

	/**
	 * Erstellt ein neues Laser-Projektil.
	 * 
	 * @param world
	 *            Welt
	 */
	public ProjectileLaser(final World world)
	{
		super();
		this.world = world;
		texture = world.res.get("ProjectileLaserRed", Texture.class);
		sprite = new Sprite(texture);

		acceleration = new Vector2(20000, 20000);
		maxSpeed = 500f;
		lifespan = 9999999;
		colissionDamage = 20;
		collisionType = CollisionType.PROJECTILE;
	}

}
