package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.Input;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;

/**
 * Von dieser Klasse erben alle Projektile.
 * 
 * @author Jakob
 * @author Sascha
 */
public abstract class Projectile extends AI {

	/**
	 * Leerer Konstruktor
	 */
	public Projectile()
	{
		super();
	}

	/**
	 * Entferne Projektil, wenn es mit etwas außer einem Moveable kollidiert ist.
	 */
	@Override
	public void collide(final Moveable target)
	{
		if (target.getIsClass() != MoveableType.COLLECTABLE)
			die();
	}

	/**
	 * Entferne Projektil, wenn es mit Spieler kollidiert ist.
	 */
	@Override
	public void collide(final Player p)
	{
		die();
	}

	/**
	 * Bewegt das Projektil nach seinen Vorgaben und aktualisiert seine Position.
	 * Ruft die überschriebene update() Methode auf.
	 */
	@Override
	public void update(final Input input, final float delta)
	{
		move(delta);
		super.update(input, delta);
	}

	/**
	 * Bewegt das Projektil.
	 * 
	 * @param delta
	 *            Zeit seit der letzten Berechnung (in ms)
	 */
	protected void move(final float delta)
	{
		velocity.add(acceleration.x * delta, 0);
		velocity.limit(maxSpeed);
	}

}
