package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * Aus dieser Klasse erben alle Interface Elemente und Objekte.
 * 
 * @author Nico
 * @author Michael
 * @author Sascha
 */
public abstract class Entity {

	/**
	 * Position der Objekte
	 */
	public Vector2 pos = new Vector2();

	/**
	 * Bilde der Elemente
	 */
	public Texture texture;

	/**
	 * Welt, in der das Objekt vorkommt
	 */
	public World world;

	/**
	 * Legt fest, ob ein Objekt gel�scht werden soll
	 */
	protected boolean deleted = false;

	/**
	 * leerer Konstruktor
	 */
	public Entity()
	{
	}

	/**
	 * Texturen entladen
	 */
	public void dispose()
	{
		texture.dispose();
	}

	/**
	 * Gibt zur�ck, ob das Objekt gel�scht werden soll
	 * 
	 * @return Wahr/Falsch
	 */
	public boolean isDeleted()
	{
		return deleted;
	}

	/**
	 * In dieser Methode wird gerendert
	 * 
	 * @param batch
	 *            : Stapeldatei, die zum Rendern ben�tigt wird
	 */
	public void render(final SpriteBatch batch)
	{
		batch.draw(texture, pos.x, pos.y);
	}

	/**
	 * 
	 * @param input
	 *            : Tastatur- und Mauseingaben
	 * @param delta
	 *            : Ber�cksichtigung der Zeitberechnung auf unterschiedlich stark laufenden Computern
	 */
	public abstract void update(Input input, float delta);

	/**
	 * In dieser Methode wird festgelegt, dass dieses Objekt gel�scht werden soll
	 */
	protected void die()
	{
		deleted = true;
	}

}
