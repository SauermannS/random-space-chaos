package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;

/**
 * Von dieser Klasse erben alle Objekte, die sich auf dem Bildschirm bewegen.
 * 
 * @author Nico
 * @author Michael
 * @author Sascha
 */
public abstract class Moveable extends Entity {

	/**
	 * Enum f�r die verschiedenen Arten/Typen der Kollision.
	 * 
	 * @author Sascha
	 */
	public enum CollisionType {
		COLLECTABLE, ENEMY, NONE, PROJECTILE
	}

	/**
	 * Enum f�r die verschiedenen Arten der "Moveable" Kinder.
	 * 
	 * @author Sascha
	 */
	public enum MoveableType {
		AI, COLLECTABLE, MOVEABLE
	}

	/**
	 * Enum f�r die verschiedenen Arten von Besitzern, die ein Moveable haben kann.
	 * 
	 * @author Sascha
	 */
	public enum Owner {
		ENEMY, NONE, PLAYER
	}

	public CollisionType collisionType = CollisionType.NONE;
	public Owner owner = Owner.NONE;

	/**
	 * Sprite (Bild)
	 */
	public Sprite sprite;

	/**
	 * Beschleunigungsvektor
	 */
	protected Vector2 acceleration = new Vector2();

	/**
	 * Zeit (in ms) bis zur L�schung des Objekts
	 */
	protected int lifespan;

	/**
	 * Maximale Geschwindigkeit (Maximale L�nge von {@link #velocity})
	 */
	public float maxSpeed;

	/**
	 * Art des Moveables. Wird verwendet zum Aufrufen der richtigen Methode.
	 */
	protected MoveableType moveableType = MoveableType.MOVEABLE;
	/**
	 * Geschwindigkeitsvektor
	 */
	protected Vector2 velocity = new Vector2();

	/**
	 * leerer Konstruktor
	 */
	public Moveable()
	{
		super();
	}

	/**
	 * Methode, welche bei Kollision ausgef�hrt wird.
	 * 
	 * @param target
	 *            Kollisionsziel
	 */
	public abstract void collide(Moveable target);

	/**
	 * Methode, welche bei Kollision mit einem Spieler ausgef�hrt wird.
	 * 
	 * @param p
	 *            Spieler
	 */
	public abstract void collide(Player p);

	public MoveableType getIsClass()
	{
		return moveableType;
	}

	/**
	 * Zeichnet das Objekt an seiner Position.
	 * �berschreibt render() in Entity, weil ein Sprite genutzt wird und keine Textur mehr.
	 */
	@Override
	public void render(final SpriteBatch batch)
	{
		sprite.setPosition(pos.x, pos.y);
		sprite.draw(batch);
	}

	/**
	 * Wenn die Lebensspanne abgelaufen ist, stirbt das Objekt.
	 * Ein Objekt darf sich nur bewegen, wenn es nicht gel�scht werden soll.
	 */
	@Override
	public void update(final Input input, final float delta)
	{
		lifespan -= delta;
		// Lebensspanne abgelaufen
		if (lifespan < 0)
			die();

		// Sachen au�erhalb des Bildschirms entfernen
		if (pos.x < -300)
			die();
		if (pos.y < -300)
			die();
		// +1000 auf x Achse damit erst gespawnte Sachen nicht schon entfernt werden
		// BUG: Bei kleineren Aufl�sungen despawnen die Sachen fr�her(im Bild)
		if ((pos.x - 1000) > (Gdx.graphics.getWidth() - sprite.getWidth()))
			die();
		if ((pos.y - 300) > (Gdx.graphics.getHeight() - sprite.getHeight()))
			die();

		if (deleted == false)
		{
			pos.x += velocity.x * delta;
			pos.y += velocity.y * delta;
		}

	}
}
