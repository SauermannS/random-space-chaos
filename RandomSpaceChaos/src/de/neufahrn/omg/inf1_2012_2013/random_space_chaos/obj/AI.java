package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

/**
 * Klasse f�r ein sich bewegendes Objekt mit Leben und Kollisionsschaden.
 * 
 * @author Jakob
 * @author Sascha
 */
public abstract class AI extends Moveable {

	/**
	 * Kollisionsschaden
	 */
	public float colissionDamage;

	/**
	 * Gesundheit
	 */
	public float health;

	/**
	 * Legt den Moveable Typ auf AI fest.
	 */
	public AI()
	{
		super();
		moveableType = MoveableType.AI;
	}
}
