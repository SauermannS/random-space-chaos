package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * @author Sascha
 * @author Michi
 * @author Niko
 * @author Jakob
 */
public class EnemyBird extends Enemy {

	/**
	 * Bewegt sich grade nach oben (true) oder unter (false)
	 */
	private boolean moveUp = true;
	/**
	 * Punkte, welche dem Spieler nach einem Kill gutgeschrieben werden
	 */
	private final int points = 10;
	/**
	 * Zeit seit dem letzten Schuss (in ms)
	 */
	public long timeLastShot;

	/**
	 * Erstellt einen neuen Gegner vom Typ Bird.
	 * 
	 * @param world
	 *            Welt
	 */
	public EnemyBird(final World world)
	{
		super();
		this.world = world;
		// Bild laden
		texture = world.res.get("EnemyBird", Texture.class);
		sprite = new Sprite(texture);

		acceleration = new Vector2(3000, 7000);
		maxSpeed = 300f;
		lifespan = 500;
		colissionDamage = 50;
		collisionType = CollisionType.ENEMY;
		timeLastShot = TimeUtils.millis() + 700;
		health = 50;
		attackDamage = 20;
	}

	/**
	 * Treffer durch Projektil: Schaden nehmen und bei Health < 0 entfernen und Explosionssound abspielen, sowie dem Spieler Punkte geben.
	 */
	@Override
	public void collide(final Moveable target)
	{
		final Sound explosion = world.res.get("SoundExplosion", Sound.class);
		final Sound hit = world.res.get("SoundHit2", Sound.class);
		if (target.getIsClass() == MoveableType.AI)
		{
			final AI targetAI = (AI) target;
			health -= targetAI.colissionDamage;
			if (health <= 0)
			{
				final Collectable c = (Collectable) world.factoryCol.spawn();
				if (c != null)
				{
					c.pos = pos;
					c.owner = Owner.ENEMY;
					world.spawnMoveable(c);
				}
				if (target.owner == Owner.PLAYER)
					world.Spieler.addPoints(points);
				die();
				explosion.play();
			} else
				hit.play();
		} else
		{
			die();
			explosion.play();
		}

	}

	/**
	 * Stirbt bei Spieler-Kollision
	 */
	@Override
	public void collide(final Player p)
	{
		die();
	}

	/**
	 * Bewegt sich in einer Schlangenbewegung (hoch/runter) nach links.
	 */
	@Override
	protected void move(final float delta)
	{
		velocity.add(-acceleration.x * delta, 0);

		if (pos.y < ((2 * Gdx.graphics.getHeight()) / 8))
			moveUp = true;
		else if ((pos.y + sprite.getHeight()) > ((7 * Gdx.graphics.getHeight()) / 8))
			moveUp = false;

		if (moveUp)
			velocity.add(0, acceleration.y * delta);
		else
			velocity.sub(0, acceleration.y * delta);

		velocity.limit(maxSpeed);
	}

	/**
	 * Schie�t zuf�llig in einem gewissen Zeitraum mit Lasern.
	 */
	@Override
	protected void shoot(final float delta)
	{
		// TODO KI
		if (timeLastShot < (TimeUtils.millis() - (int) ((Math.random() * (1500 - 500)) + 499)))
		{
			final Projectile laser = new ProjectileLaser(world);
			laser.colissionDamage = attackDamage;
			laser.pos.x = pos.x + (laser.sprite.getWidth() / 4);
			laser.pos.y = (pos.y + (sprite.getHeight() / 2)) - (laser.sprite.getHeight() / 2);
			laser.owner = Owner.ENEMY;
			laser.world = world;
			laser.acceleration.x = -laser.acceleration.x;
			world.spawnMoveable(laser);
			timeLastShot = TimeUtils.millis();
		}
	}

}
