package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * Collectable gibt dem Spieler Punkte, wenn es aufgesammelt wird.
 * 
 * @author Sascha
 */
public class CollectablePoints extends Collectable {

	/**
	 * Erstellt ein neues Punkte-Collectable.
	 * 
	 * @param world
	 *            Welt
	 */
	public CollectablePoints(final World world)
	{
		super();
		type = Type.POINTS;

		// Bild laden
		texture = world.res.get("CollectableInstantPoints", Texture.class);
		// Bildausschnitt wählen TextureRegion(texture, X: (Kachel-1)*Kachelgröße, Y: (Kachel-1)*Kachelgröße, Kachelgröße, Kachelgröße)
		// TextureRegion region = new TextureRegion(texture, 2*128, 0, 128, 128);
		// Sprite erstellen und zuweisen
		sprite = new Sprite(texture);

		acceleration = new Vector2(-9999999, 0);
		maxSpeed = 300f;
		lifespan = 9999999;
		collisionType = CollisionType.COLLECTABLE;
		owner = Owner.ENEMY;
		strength = 50;
	}
}
