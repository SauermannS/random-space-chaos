package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * Collectable gibt f�r x Sekunden einen Doppelschuss mit St�rke y, wenn es aufgesammelt wird.
 * 
 * @author Sascha
 */
public class CollectableDoubleLaser extends Collectable {

	/**
	 * Zus�tzliche Punkte, neben dem normalen Effekt
	 */
	private final int points = 5;

	/**
	 * Wirkzeit in ms
	 */
	private final int time = 1000;

	/**
	 * Erstellt ein neues DoubleLaser-Collectable.
	 * 
	 * @param world
	 */
	public CollectableDoubleLaser(final World world)
	{
		super();
		type = Type.DOUBLELASER;

		// Bild laden
		texture = world.res.get("CollectableInstantAttackDamage", Texture.class);
		// Sprite erstellen und zuweisen
		sprite = new Sprite(texture);

		acceleration = new Vector2(-9999999, 0);
		maxSpeed = 300f;
		lifespan = 9999999;
		collisionType = CollisionType.COLLECTABLE;
		owner = Owner.ENEMY;
		strength = 20;
	}

	/**
	 * F�gt dem Spieler bei der Kollision zus�tzlich Punkte hinzu. F�hrt �berschriebene collide() Methode aus.
	 */
	@Override
	public void collide(final Player p)
	{
		p.addPoints(points);
		super.collide(p);
	}

	/**
	 * Gibt die Wirkzeit des Collectables zur�ck
	 * 
	 * @return Wirkzeit in ms
	 */
	public int getTime()
	{
		return time;
	}
}
