package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * Collectable repariert die H�lle des Spielers, wenn es aufgesammelt wird.
 * 
 * @author Jakob
 * @author Sascha
 */
public class CollectableRepair extends Collectable {

	/**
	 * Zus�tzliche Punkte, neben dem normalen Effekt
	 */
	private final int points = 5;

	/**
	 * Erstellt ein neues H�llenreperatur-Collectable.
	 * 
	 * @param world
	 */
	public CollectableRepair(final World world)
	{
		super();
		type = Type.HULLREPAIR;

		// Bild laden
		texture = world.res.get("CollectableInstantRepairHull", Texture.class);
		// Sprite erstellen und zuweisen
		sprite = new Sprite(texture);

		acceleration = new Vector2(-9999999, 0);
		maxSpeed = 300f;
		lifespan = 9999999;
		collisionType = CollisionType.COLLECTABLE;
		owner = Owner.ENEMY;
		strength = 35;
	}

	/**
	 * F�gt dem Spieler bei der Kollision zus�tzlich Punkte hinzu. F�hrt �berschriebene collide() Methode aus.
	 */
	@Override
	public void collide(final Player p)
	{
		p.addPoints(points);
		super.collide(p);
	}
}
