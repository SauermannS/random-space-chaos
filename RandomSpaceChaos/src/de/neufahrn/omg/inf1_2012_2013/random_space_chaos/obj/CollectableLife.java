package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;
import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.World;

/**
 * Collectable gibt dem Spieler Leben, wenn es aufgesammelt wird.
 * 
 * @author Sascha
 */
public class CollectableLife extends Collectable {

	/**
	 * Zus�tzliche Punkte, neben dem normalen Effekt
	 */
	private final int points = 5;

	/**
	 * Erstellt ein neues Life-Collectable.
	 * 
	 * @param world
	 */
	public CollectableLife(final World world)
	{
		super();
		type = Type.LIFE;

		// Bild laden
		texture = world.res.get("CollectableInstantAddLife", Texture.class);
		// Sprite erstellen und zuweisen
		sprite = new Sprite(texture);

		acceleration = new Vector2(-9999999, 0);
		maxSpeed = 300f;
		lifespan = 9999999;
		collisionType = CollisionType.COLLECTABLE;
		owner = Owner.ENEMY;
		strength = 1;
	}

	/**
	 * F�gt dem Spieler bei der Kollision zus�tzlich Punkte hinzu. F�hrt �berschriebene collide() Methode aus.
	 */
	@Override
	public void collide(final Player p)
	{
		p.addPoints(points);
		super.collide(p);
	}
}
