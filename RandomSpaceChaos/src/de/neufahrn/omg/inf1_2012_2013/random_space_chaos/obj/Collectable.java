package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj;

import com.badlogic.gdx.Input;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.Player;

/**
 * Von dieser Klasse erben alle aufsammelbaren Objekte.
 * 
 * @author Michi
 * @author Niko
 * @author Sascha
 */
public abstract class Collectable extends Moveable {

	/**
	 * Typ des Collectables
	 * 
	 * @author Sascha
	 */
	public enum Type {
		HULLREPAIR, LIFE, NONE, POINTS, SHIELDBOOST, DOUBLELASER
	}

	/**
	 * St�rke des Effekts, das dieses Collectable auf einen Spieler hat.
	 */
	public float strength;

	/**
	 * Art des Effektes, das dieses Collectable auf einen Spieler hat.
	 */
	public Type type = Type.NONE;

	/**
	 * Setzt die Art des Moveables auf Collectable.
	 */
	public Collectable()
	{
		super();
		moveableType = MoveableType.COLLECTABLE;
	}

	/**
	 * Kein Effekt bei Kollision mit anderen Gegenst�nden
	 */
	@Override
	public void collide(final Moveable target)
	{
	}

	/**
	 * L�schen, wenn Kollision mit Spieler (aufsammeln)
	 */
	@Override
	public void collide(final Player p)
	{
		die();
	}

	/**
	 * Aktualisiert die Position des Collectables
	 */
	@Override
	public void update(final Input input, final float delta)
	{
		move();
		super.update(input, delta);
	}

	/**
	 * Bewegt sich mit konstanter Geschwindigkeit nach links.
	 */
	protected void move()
	{
		velocity.x = -maxSpeed;
	}
}
