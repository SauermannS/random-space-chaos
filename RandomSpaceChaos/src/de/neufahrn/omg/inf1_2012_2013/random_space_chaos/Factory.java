package de.neufahrn.omg.inf1_2012_2013.random_space_chaos;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.obj.Moveable;

/**
 * K�mmert sich um das Spawnen von Moveables.
 * 
 * @author Sascha
 * @param <T>
 *            Typ
 */
public class Factory<T extends Moveable> {

	/**
	 * Liste aller spawnbaren Objekte und ihrer Wahrscheinlichkeiten.
	 */
	private final List<SpawnChance<? extends Moveable>> spawns;

	/**
	 * Welt
	 */
	private final World world;

	/**
	 * Erstellt eine neue Fabrik.
	 * 
	 * @param world
	 *            Welt
	 */
	public Factory(final World world)
	{
		spawns = new ArrayList<SpawnChance<? extends Moveable>>();
		this.world = world;
	}

	/**
	 * F�gt der Liste eine neue Klasse-Wahrscheinlichkeits Kombination hinzu.
	 * 
	 * @param sc
	 *            SpawnChance
	 */
	public void addSpawnChance(final SpawnChance<? extends Moveable> sc)
	{
		spawns.add(sc);
	}

	/**
	 * W�hlt ein Objekt aus der Liste aus und gibt ein zu spawnendes Objekt zur�ck.
	 * 
	 * @return Objekt/null
	 */
	public Object spawn()
	{
		final Random r = new Random();
		final SpawnChance<?> selectedObj = spawns.get(r.nextInt(spawns.size()));
		if (selectedObj.shouldSpawn())
			try
			{
				return selectedObj.getClassName().getConstructor(World.class).newInstance(world);
			} catch (final InstantiationException e)
			{
				e.printStackTrace();
			} catch (final IllegalAccessException e)
			{
				e.printStackTrace();
			} catch (final IllegalArgumentException e)
			{
				e.printStackTrace();
			} catch (final InvocationTargetException e)
			{
				e.printStackTrace();
			} catch (final NoSuchMethodException e)
			{
				e.printStackTrace();
			} catch (final SecurityException e)
			{
				e.printStackTrace();
			}
		return null;
	}
}
