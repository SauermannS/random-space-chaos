# README #

Random Space Chaos (RSC) entstand 2013 als Projekt im Informatikunterricht (11. Klasse) am Oskar-Maria-Graf Gymnasium Neufahrn.

### Verwendete Technologien ###

* LibGDX Framework (http://libgdx.badlogicgames.com/)
* sfxr für Soundeffekte (http://www.drpetter.se/project_sfxr.html)
* Grafiken und Musik sind extra selbst für RSC erstellt

### Spiel ausprobieren ###

* Als Downloads sind kompilierte und lauffähige Versionen von RSC verfügbar.
* Der Download liegt als Zip-Archiv vor (ggf. Dateiendung wieder anfügen)

### Steuerung ###
* Bewegen über die Pfeiltasten
* Schießen mit Leertaste
* Enter startet das Spiel neu
* ESC pausiert das Spiel

### Screenshot ###
![RandomSpaceChaosScreenshot.PNG](https://bitbucket.org/repo/bxqpB9/images/3882507372-RandomSpaceChaosScreenshot.PNG)