package de.neufahrn.omg.inf1_2012_2013.random_space_chaos.client;

import de.neufahrn.omg.inf1_2012_2013.random_space_chaos.RandomSpaceChaos;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class GwtLauncher extends GwtApplication {
	@Override
	public GwtApplicationConfiguration getConfig () {
		GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(1280, 720);
		return cfg;
	}

	@Override
	public ApplicationListener getApplicationListener () {
		return new RandomSpaceChaos();
	}
}